package com.mobileallin.apidor.network;

import com.mobileallin.apidor.data.model.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Dawid on 2017-08-16.
 */

public interface UnsplashAPI {

    String BASE_URL = "https://unsplash.it/";

    @GET("/list")
    Call<List<Photo>> getAllPhotos();
}

