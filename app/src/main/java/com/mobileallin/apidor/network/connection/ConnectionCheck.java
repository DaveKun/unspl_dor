package com.mobileallin.apidor.network.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by Dawid on 2017-08-17.
 */
/*Check internet connection, notify if no connection*/
public class ConnectionCheck {

    public BroadcastReceiver internetConnectionReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    /*connected to wifi*/
                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    /*connected to the mobile provider's data plan*/
                }
            } else {
                Toast.makeText(context, "No Internet connection!", Toast.LENGTH_LONG).show();
            }
        }
    };
}
