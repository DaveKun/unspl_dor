package com.mobileallin.apidor.ui.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.bumptech.glide.Glide;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.mobileallin.apidor.R;
import com.mobileallin.apidor.data.model.Photo;
import com.mobileallin.apidor.databinding.PhotoListItemBinding;
import com.mobileallin.apidor.ui.utils.SharedPrefUtils;

import java.util.ArrayList;

/**
 * Created by Dawid on 2017-08-16.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> implements Filterable {

    private ArrayList<Photo> photos;
    private LayoutInflater layoutInflater;
    private final int requiredPhotoWidth;
    private int requiredPhotoHeight;
    private SharedPrefUtils sharedPreferencesMethods;

    /*searching related*/
    private PhotoFilter photoFilter;
    private ArrayList<Photo> filteredPhotosList;
    private MaterialFavoriteButton favBtn;

    public PhotoAdapter(@NonNull ArrayList<Photo> photos, @NonNull Context context) {
        this.photos = photos;
        this.filteredPhotosList = photos;

        getFilter();

        sharedPreferencesMethods = new SharedPrefUtils();

        layoutInflater = LayoutInflater.from(context);
        requiredPhotoWidth = context.getResources().getDisplayMetrics().widthPixels;
        requiredPhotoHeight = context.getResources().getDisplayMetrics().heightPixels;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoViewHolder((PhotoListItemBinding) DataBindingUtil.inflate(layoutInflater,
                R.layout.photo_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        PhotoListItemBinding binding = holder.getBinding();
        Photo data = filteredPhotosList.get(position);
        binding.setData(data);
        binding.executePendingBindings();

        favBtn = binding.favBtn;
        /*Code for selecting only one item as favourite (recycler bug)*/
        if (sharedPreferencesMethods.getFromSP("favourite" + String.valueOf(position), layoutInflater.getContext())) {
            favBtn.setFavorite(true);
        } else {
            /*all other items item - leave unmarked*/
            favBtn.setFavorite(false);
        }

        final int finalPos = position;

        /**For favBtn bug - not to get checked on multiple recycler items*/
        favBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*this means the btn was clicked twice - uncheck the favourite state,
                delete the state from Shared Prefs*/
                if (sharedPreferencesMethods.getFromSP("favourite" + String.valueOf(finalPos), layoutInflater.getContext())) {
                    favBtn.setFavorite(false);
                    sharedPreferencesMethods.deleteFromSP("favourite" + String.valueOf(finalPos), layoutInflater.getContext());
                } else {
                    sharedPreferencesMethods.saveInSP("favourite" + String.valueOf(finalPos), true, layoutInflater.getContext());
                    favBtn.setFavorite(true);
                }
                notifyDataSetChanged();
            }
        });

        Glide.with(layoutInflater.getContext())
                .load(holder.getBinding().getData().getPhotoUrl(requiredPhotoWidth, requiredPhotoHeight))
                .placeholder(R.color.imagePlaceholder)
                .into(holder.getBinding().photoThumbnail);
    }

    @Override
    public int getItemCount() {
        int photosListSize ;

        if(filteredPhotosList != null && !filteredPhotosList.isEmpty()) {

            photosListSize = filteredPhotosList.size();
        }
        else {
            photosListSize = 0;
        }
        return photosListSize;
    }

    @Override
    public long getItemId(int position) {
        return filteredPhotosList.get(position).id;
    }

    public ArrayList<Photo> getFilteredPhotosList() {
        return filteredPhotosList;
    }

    @Override
    public Filter getFilter() {
        if (photoFilter == null) {
            photoFilter = new PhotoFilter();
        }

        return photoFilter;
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private final PhotoListItemBinding binding;

        PhotoViewHolder(PhotoListItemBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
        }

        public PhotoListItemBinding getBinding() {
            return binding;
        }

        @Override
        public void onClick(View view) {
        }
    }

    /**
     * Custom filter for photos list
     * Filter content in photo list according to the search text
     */
    private class PhotoFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<Photo> tempList = new ArrayList<Photo>();

                /**search content in photos list*/
                for (Photo photo : photos) {
                    if (photo.author.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(photo);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = photos.size();
                filterResults.values = photos;
            }

            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         *
         * @param constraint text
         * @param results    filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredPhotosList = (ArrayList<Photo>) results.values;
            notifyDataSetChanged();
        }
    }
}