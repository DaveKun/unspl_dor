package com.mobileallin.apidor.ui.utils;

/**
 * Created by Dawid on 2017-08-16.
 */

public class IntentUtils {

    public static final String PHOTO = "photo";
    public static final String RELEVANT_PHOTOS = "relevant";
    public static final String SELECTED_ITEM_POSITION = "selected";

}
