package com.mobileallin.apidor.ui;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.mobileallin.apidor.R;
import com.mobileallin.apidor.data.model.Photo;
import com.mobileallin.apidor.network.UnsplashAPI;
import com.mobileallin.apidor.network.connection.ConnectionCheck;
import com.mobileallin.apidor.ui.adapter.PhotoAdapter;
import com.mobileallin.apidor.ui.utils.IntentUtils;
import com.mobileallin.apidor.ui.utils.PhotoComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PhotosListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private ArrayList<Photo> mPhotos;
    private ProgressBar progressBar;
    private PhotoAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ConnectionCheck internetConnectionCheck;
    private Photo photoModel;
    private UnsplashAPI unsplashAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        internetConnectionCheck = new ConnectionCheck();
        photoModel = new Photo();

        setContentView(R.layout.activity_photos_list);

        progressBar = findViewById(R.id.progress);
        mRecyclerView = findViewById(R.id.photos_list);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        /*Setting an empty adapter, before we load the data
        (otherwise E/RecyclerView: No adapter attached; skipping layout)*/
        mAdapter = new PhotoAdapter(mPhotos, this);
        mRecyclerView.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            mPhotos = savedInstanceState.getParcelableArrayList(IntentUtils.RELEVANT_PHOTOS);
        }

        progressBar.setVisibility(View.VISIBLE);

        /*Get the callback, fill in the view if successful controller*/
        createUnsplashAPI();
        unsplashAPI.getAllPhotos().enqueue(answersCallback);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(IntentUtils.RELEVANT_PHOTOS, mPhotos);
        super.onSaveInstanceState(outState);
    }

    Callback<List<Photo>> answersCallback = new Callback<List<Photo>>() {
        @Override
        public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
            if (response.isSuccessful()) {
                mPhotos = new ArrayList<>(response.body());

                Collections.sort(mPhotos, new PhotoComparator());

                photoModel.generateAuthorId(0, mPhotos, mPhotos.size());

                fillInTheView(mPhotos);
            } else {
                Toast.makeText(getApplicationContext(),
                        "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<List<Photo>> call, Throwable t) {
            Toast.makeText(getApplicationContext(), "Error. Could not load the latest data", Toast.LENGTH_SHORT).show();
        }
    };

    private void createUnsplashAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UnsplashAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        unsplashAPI = retrofit.create(UnsplashAPI.class);
    }

    public void fillInTheView(ArrayList<Photo> photos) {

        /*We go the data - it's time to set the final adapter*/
        mAdapter = new PhotoAdapter(photos, this);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new OnItemClickListener(PhotosListActivity.this) {
            @Override
            public void onItemSelected(RecyclerView.ViewHolder holder, int position) {
                ArrayList<Photo> sortedPhotos = mAdapter.getFilteredPhotosList();

                final Intent intent = getDetailActivityStartIntent(PhotosListActivity.this,
                        sortedPhotos, position);

                PhotosListActivity.this.startActivityForResult(intent, 123);
            }
        });
        progressBar.setVisibility(View.GONE);
    }

    @NonNull
    private static Intent getDetailActivityStartIntent(Activity host, ArrayList<Photo> photos,
                                                       int position) {
        final Intent intent = new Intent(host, PhotoDetailActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.putParcelableArrayListExtra(IntentUtils.PHOTO, photos);
        intent.putExtra(IntentUtils.SELECTED_ITEM_POSITION, position);
        return intent;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(internetConnectionCheck.internetConnectionReciever, new IntentFilter(
                "android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(internetConnectionCheck.internetConnectionReciever);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         /*Inflate the options menu from XML*/
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

         /*Get the SearchView and set the searchable configuration*/
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
         /*Assumes current activity is the searchable activity*/
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);  /*Do not iconify the widget; expand it by default TEST*/

        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String searchText) {
        if (mAdapter == null) {
            return false;
        }
        mAdapter.getFilter().filter(searchText);
        return true;
    }
}

