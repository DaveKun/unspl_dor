package com.mobileallin.apidor.ui;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.mobileallin.apidor.R;
import com.mobileallin.apidor.data.model.Photo;
import com.mobileallin.apidor.network.connection.ConnectionCheck;
import com.mobileallin.apidor.ui.pager.DetailPagerAdapter;
import com.mobileallin.apidor.ui.utils.IntentUtils;

import java.util.ArrayList;

/**
 * Created by Dawid on 2017-08-16.
 */

public class PhotoDetailActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private int initialItem;
    private ConnectionCheck internetConnectionCheck;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_detail_pager);

        progressBar = findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);

        internetConnectionCheck = new ConnectionCheck();
        Intent intent = getIntent();

        initialItem = intent.getIntExtra(IntentUtils.SELECTED_ITEM_POSITION, 0);
        setUpViewPager(intent.<Photo>getParcelableArrayListExtra(IntentUtils.PHOTO));
        progressBar.setVisibility(View.VISIBLE);

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void setUpViewPager(ArrayList<Photo> photos) {
        viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(new DetailPagerAdapter(photos, this, PhotoDetailActivity.this));
        viewPager.setCurrentItem(initialItem);

        viewPager.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(internetConnectionCheck.internetConnectionReciever, new IntentFilter(
                "android.net.conn.CONNECTIVITY_CHANGE"));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(internetConnectionCheck.internetConnectionReciever);
    }
}
