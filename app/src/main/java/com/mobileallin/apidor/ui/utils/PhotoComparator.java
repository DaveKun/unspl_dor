package com.mobileallin.apidor.ui.utils;

import com.mobileallin.apidor.data.model.Photo;

import java.util.Comparator;

/**
 * Created by Dawid on 2017-08-18.
 */
/*Sort the Collection by author name*/
public class PhotoComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        Photo p1;
        Photo p2;
        if (o1 instanceof Photo && o2 instanceof Photo) {
            p1 = (Photo) o1;
            p2 = (Photo) o2;
        } else {
            return 0;
        }
        return p1.author.compareToIgnoreCase(p2.author);
    }
}
