package com.mobileallin.apidor.ui.pager;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.mobileallin.apidor.R;
import com.mobileallin.apidor.data.model.Photo;
import com.mobileallin.apidor.databinding.ActivityPhotoDetailBinding;

import java.util.List;

/**
 * Created by Dawid on 2017-08-17.
 */

public class DetailPagerAdapter extends PagerAdapter {

    private List<Photo> allPhotos;
    private LayoutInflater layoutInflater;
    private Activity host;

    private int requiredPhotoWidth;
    private int requiredPhotoHeight;

    public DetailPagerAdapter(@NonNull List<Photo> allPhotos, @NonNull Activity host,
                              @NonNull Context context) {
        this.allPhotos = allPhotos;
        this.layoutInflater = LayoutInflater.from(host);
        this.host = host;

        /*Getting the required photo size by Glide from the API*/
        requiredPhotoWidth = context.getResources().getDisplayMetrics().widthPixels;
        requiredPhotoHeight = context.getResources().getDisplayMetrics().heightPixels;
    }

    @Override
    public int getCount() {
        return allPhotos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object instanceof ActivityPhotoDetailBinding
                && view.equals(((ActivityPhotoDetailBinding) object).getRoot());
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ActivityPhotoDetailBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.activity_photo_detail, container, false);
        binding.setData(allPhotos.get(position));
        onViewBound(binding);
        binding.executePendingBindings();
        container.addView(binding.getRoot());
        return binding;
    }

    private void onViewBound(ActivityPhotoDetailBinding binding) {
        Glide.with(host)
                .load(binding.getData().getPhotoUrl(requiredPhotoWidth, requiredPhotoHeight)).asBitmap().format(DecodeFormat.PREFER_ARGB_8888)
                .placeholder(R.color.imagePlaceholder)
                .into(binding.detailPhoto);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(((ActivityPhotoDetailBinding) object).getRoot());
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {

    }
}