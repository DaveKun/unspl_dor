package com.mobileallin.apidor.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Dawid on 2017-08-16.
 */

public class Photo implements Parcelable, Serializable {

    private String format;
    public int width;
    public int height;
    private String filename;
    public long id;
    public int author_id;
    public String author;
    private String author_url;
    private String post_url;

    private static final String PHOTO_URL_BASE = "https://unsplash.it/%d/%d?image=%d";

    public Photo() {
    }

    public Photo(String format,
                 int width,
                 int height,
                 String filename,
                 long id,
                 int author_id,
                 String author,
                 String author_url,
                 String post_url
    ) {
        this.format = format;
        this.width = width;
        this.height = height;
        this.filename = filename;
        this.id = id;
        this.author_id = author_id;
        this.author = author;
        this.author_url = author_url;
        this.post_url = post_url;
    }

    protected Photo(Parcel in) {
        format = in.readString();
        width = in.readInt();
        height = in.readInt();
        filename = in.readString();
        id = in.readLong();
        author_id = in.readInt();
        author = in.readString();
        author_url = in.readString();
        post_url = in.readString();
    }


    public String getPhotoUrl(int requestWidth, int requestHeight) {
        return String.format(Locale.getDefault(), PHOTO_URL_BASE, requestWidth, requestHeight, id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(format);
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeString(filename);
        dest.writeLong(id);
        dest.writeInt(author_id);
        dest.writeString(author);
        dest.writeString(author_url);
        dest.writeString(post_url);
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    public long getId() {
        return id;
    }

    private long getAuthorId() {
        return author_id;
    }

    private void setAuthorId(int authorId) {
        this.author_id = authorId;
    }

    /*Custom method for generating author_id - the picture number of a given author*/
    public void generateAuthorId(int startingPoint, ArrayList<Photo> allPhotos, int photoListArraySize) {
        for (int i = startingPoint; i < photoListArraySize - 1; i++) {
            int z;
            if (i - 1 < 0) {
                allPhotos.get(i).setAuthorId(1);
                continue;
            }
            if (allPhotos.get(i).author.equalsIgnoreCase(allPhotos.get(i - 1).author)) {
                z = i;
                if (allPhotos.get(i - 1).getAuthorId() < i) {
                    z = (int) allPhotos.get(i - 1).getAuthorId();
                }
                allPhotos.get(i).setAuthorId(z + 1);
            } else {
                allPhotos.get(i).setAuthorId(1);
            }
        }
    }
}
