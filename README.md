# README #

### What is this repository for? ###

The application fetches the data from the https://unsplash.it. The items are shown as a RecyclerView list. 
Each list item includes the photo thumbnail, the author full name, the photo number of the author and a favourite button. 

### Application functionalities ###

1. Connection with the chosen API - unslpash.it via Retrofit2
2. Showing the API data in a form of a list
3. Taking advantage of the master-detail flow (getting to the detail view from each list item)
4. Searching functionality - search by the author name
5. Marking the favourite photos - the "favourite" state is saved in the Shared Preferences
6. Changing the view in the landscape mode
7. Changing the item (photo with a number, author full name and size) in detail view by swiping through the list
8. Zooming / pitching each image in detail view (Chris Banes PhotoView library)
9. Listening for no internet connection and displaying a warning Toast

### Chosen concepts and solutions ###

1. Retrofit 2 is used for fetching the data from the https://unsplash.it. 
2. The data are shown as a RecyclerView list.
3. Marking the favourite photos the favourite button - ivbaranov MaterialFavoriteButton library. 
   The state is saved in SharedPreferences in the adapter.
4. The screen slides - ViewPager, PagerAdapter (DetailPagerAdapter class)
5. Zooming / pitching the photo in detail view - Chris Banes PhotoView library
6. Listening for no internet connection - BroadcastReceiver defined in ConnectionCheck class.

What sill needs to be done ?
Favourite button, although generally works, still has some bugs
Reorganising the code to separate the logic out of view components (using the MVP pattern).  

### How do I get set up? ###

Just open the app in Android Studio and you're good to go. No authorisation key from the unsplah.it required.

### Who do I talk to? ###

Dawid Kunicki
dawidkunicki88@gmail.com